# Machine Learning Exercise 1



Julian Krieger, 3761258

Roman Pinkel, 3761932

## 1.1 Feature Types and Feature Encoding (2PT)



### Task 1



Provided with the following table:

![image-20200509140547164](./Machine Learning Exercise 1.assets/image-20200509140547164.png)









And its corresponding information

![image-20200509140615068](./Machine Learning Exercise 1.assets/image-20200509140615068.png)



#### a) Decide for each of the above features wether it is *nominal, ordinal,* or *numerical*

1. Zip Code: **Either Numerical or Nominal Feature**
   1. Could perhaps be a **numerical feature**, however comparison of zip codes only makes sense if similar zip codes are located relatively near each other (e.g every ZIP Code can be assigned to clusters ranging from the least significant digit towards the most significant digit). Since I am not sure, I think ZIP Codes are more of an Identifier than something with numerical value. 
2. Revenue: **Numerical Feature**
3. Registered: **Numerical Feature**
4. Complaints: **Numerical Feature**
5. Category: **Nominal Feature**
6. Gender: **Nominal Feature**
7. Level: **Ordinal Features**



#### b) Encode the above data in such a way that it can be used in a regression analysis. Please state which additional feature are required, to encode nominal and ordinal features, and provide a table with the corresponding values for the four customers



##### Nominal Features

Can be encoded with **One-Hot encoding**



##### Ordinal Features

Can be mapped to **integer values**



##### Table



| ZIP   | Revenue | Registered | Compl. | C~Books~ | C~Electronics~ | C~Clothing~ | C~Food~ | G~M~ | G~F~ | LVL  |
| ----- | ------- | ---------- | ------ | -------- | -------------- | ----------- | ------- | ---- | :--- | :--- |
| 66121 | 5990.23 | 2013       | 2      | 1        | 0              | 0           | 0       | 0    | 1    | 2    |
| 80331 | 517.89  | 2016       | 4      | 0        | 1              | 0           | 0       | 1    | 0    | 4    |
| 51149 | 8972.34 | 2010       | 7      | 0        | 0              | 1           | 0       | 1    | 0    | 3    |
| 21073 | 131.67  | 2017       | 0      | 0        | 0              | 0           | 1       | 0    | 1    | 1    |
| 66386 | 511.24  | 2015       | 1      | 1        | 0              | 0           | 0       | 0    | 1    | 2    |





#### c) Often it is useful to derive additional features from the original data – a process known as feature engineering. Assume that you have external data indicating the state in which a zip code is located (e.g., 66121 in Saarland). Also consider that Revenue and Complaints depend on how long someone has been a customer. Which additional features would you add?



1. Average Complaints per Year
   1. To give a meaning to Complaints in Relation to the Year the Customer registered, divide the total number of complaints by $Year_{Current} - Year_{Registered}$
2. Average Revenue per Year







### Task 2



