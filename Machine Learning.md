



[TOC]



## 1. Introduction

### 1.1 Structured Data

![image-20200509141720715](Machine Learning.assets/image-20200509141720715.png)



### 1.2 Unstructured and Semi-Structured Data

- Example: texts written by humans
- Sometimes semi-structured (interleaving structured and unstructured data)

![image-20200509141823234](Machine Learning.assets/image-20200509141823234.png)



### 1.3 Feature Types

- We distinguish different **types of features** depending on their operations which can be applied



![image-20200509142014417](Machine Learning.assets/image-20200509142014417.png)



#### Nominal Features

- Can be **compared** (=, !=) and **counted**
  - Example: Gender of a person, color of a car



#### Ordinal features

- Can **in addition** be **compared** (<, >)
  - Example: Customer satisfaction level, energy class of car



#### Numerical features

- **in addition** allow for **arithmetic operations** (+, - ,*, %)
  - Allows computation of difference between values
  - Allows computation of mean, variance, etc



#### Example

![image-20200509142249281](Machine Learning.assets/image-20200509142249281.png)



### 1.4 Supervised vs. Unsupervised Learning



#### Supervised Learning

- assume that **training data** is available from which they can learn to **predict** a **target feature**, ***based on other features***



#### Unsupervised Learning

- Take a given dataset and aim at **gaining insights** by **identifying patterns** 
  - Example: Grouping similar data points



### 1.5 Types of Machine Learning Algorithms

1. Regression
   - Predict a numerical target feature based on one or multiple other (numerical) features
2. Classification
   - Predict a **nominal target feature** based on others (email as spam, ...)
3. Clustering
   - Reveals the structure of data by **grouping data points** so that similar ones stay together and dissimilar ones are seperated
   - Example: Identify segments of customers, find communities in a social network





## 2. Regression

- predict a **numerical target feature** based on *one* or *multiple* other (numerical) features
- Ex: Predict consumption of a card based on its power



### 2.1 Ordinary Least Squares

- How to predict a **numerical feature *y*** based on the value of **another numerical feature *x***



1. Make **assumption** about their **relationship** (how x influences y)
2. Example Model: $$ŷ=\omega_0 + \omega_1x$$ (linear)
3. **Determine the coefficients** $\omega_0$ and $\omega_1$

![image-20200509132214363](Machine Learning.assets/image-20200509132214363.png)







#### Data Points

- Determine values of $\omega_0$ and $\omega_1$ based on **training data**
- **Training Data: ** **n** data points $(x_i, y_i)$
- In Example: **pairs of power **(in horsepower) and **fuel consumption** (in mpg) of individual cars



#### Mean

Mean of $x$ and $y$ : 

![image-20200509132431506](Machine Learning.assets/image-20200509132431506.png)



#### Variance

of $x$ and $y$: 

![image-20200509132502803](Machine Learning.assets/image-20200509132502803.png)



Values $\sigma_x^2$ and $\sigma_y^2$ are called **Standard Deviation of $x$ and $y$**



#### Covariance

**Covariance** $cov_{x,y}$ measures degree of **joint variability**

![image-20200509132727808](Machine Learning.assets/image-20200509132727808.png)



- **Large Covariance** suggests that two features vary jointly
  - **positive value** indicates deviation from their respective mean in **the same direction**
  - **negative value** indicated deviation from their respective mean in **opposite directions**





#### Pearson Correlation Coefficient

-> **normalized measure of linear correlation **between $x$ and $y$

![image-20200509132918342](Machine Learning.assets/image-20200509132918342.png)



- Takes values in $[-1, +1]$
  - **-1** indicates **negative linear correlation**
  - **0** indicates that there is **no linear correlation**
  - **1** indicates a **positive linear correlation**



#### Loss Function

- measures how well our model - for a specific choice of **coefficients** $w_0$ and $w_1$ describes the training data 
  - -> how much is lost by our model
- **Residual** for data point $(x_i, y_i)$ measures how much the **observed value $y_i$ differs from the prediction of our model**
  - ![image-20200509133320029](Machine Learning.assets/image-20200509133320029.png)



#### Ordinary least squares (OLS)

- uses **sum of squared residuals** (also: sum of squared errors) as a loss function
  - ![image-20200509134747924](Machine Learning.assets/image-20200509134747924.png)
- We are interested in finding **the coefficients $\omega_0$ and $\omega_1$** that minimize the loss -> **Optimization Problem**
  - ![image-20200509134856665](Machine Learning.assets/image-20200509134856665.png)
  - We are actually not interested in the result of the minimal loss function, but **at the miminum of $w_0$ and $w_1$ ** that **lead to that minimum**

#### Determining Optimal Coefficients Analytically

- Optimal Values for **coefficients $\omega_10$ and $\omega_1$** can be determined analytically



1. Compute **partial derivatives** of loss function w.r.t $\omega_0$ and $\omega_1$

   ![image-20200509135108068](Machine Learning.assets/image-20200509135108068.png)

2. identify **common zero** by solving systems of equations

   ![image-20200509135135236](Machine Learning.assets/image-20200509135135236.png)



##### Closed-Form

![image-20200509135218948](Machine Learning.assets/image-20200509135218948.png)

![image-20200509135226985](Machine Learning.assets/image-20200509135226985.png)





#### $R^2$ Coefficient of Determination

- Measures **how well** the determined **regression line approximates the data**
- **Or:** how much of the variation observed in the data is explained by it



![image-20200509135617285](Machine Learning.assets/image-20200509135617285.png)



### 2.2 Gradiant Descent



- Not **always possible** to determine **optimal coefficients** for a loss function **analytically**
- -> **Gradient descent** = optimization algorithm to determine the minimum of a loss function



#### Idea

- Start with **random choice** of coefficients (here: $\omega_0$ and $\omega_1$)
- **repeat** for a specified number of rounds or until convergence
  - Compute the **gradient** for this choice of coefficients
  - **update coefficients** based on gradient



#### Intuition

- Loss function as a **surface** and which we find the **lowest point**	
  - Start journey at **random position**
  - **repeat: **
    - identify **direction with steepest descent**
    - **walk** a **few steps** in the identified direction

#### Implementation

- **multivariable function** defined as the **vector** of its **partial derivates** when evaluated at a **specific point**, it indicates the **direction of steepest ascent**
- For our loss function, the gradient is thus defined as 
  - ![image-20200509140226514](./Machine Learning.assets/image-20200509140226514.png)
- We think of our current choice **of coefficients as a vector**
  - ![image-20200509140254229](./Machine Learning.assets/image-20200509140254229.png)
- Coefficients are **updated** in each round as 
  - ![image-20200509140311064](./Machine Learning.assets/image-20200509140311064.png)
  - with $0 < \alpha \le 1$ as the **learning rate** (or step size)



### 2.3 Multiple Linear Regression

- Prediction of a **numerical feature $y$** based on **multiple** other numerical features $x_1, ..., x_m$
- Assuming a **linear relationship** between target feature and the other ones
  - ![image-20200509155647893](Machine Learning.assets/image-20200509155647893.png)
- Because of **multiple data points and multiple features**, we now use **matrices and vectors**





#### Data Matrix

- **NxM Matrix X**
- **Rows** = **data points**
- **Columns** = **features**



![image-20200509160046321](Machine Learning.assets/image-20200509160046321.png)



##### Intercept Coefficient $\omega_0$

- If we want to determine this, the **first column** of the matrix is assumed to **only be 1s**



##### Target Vector

- provided as a **n x 1 vector**
- ![image-20200509160155470](Machine Learning.assets/image-20200509160155470.png)



##### Coefficient Vector

- Goal is to determine an optimal $m$ x 1​ vector
- ![image-20200509160229757](Machine Learning.assets/image-20200509160229757.png)

#### Prediction Vector and Loss Function

- Multiplication of data matrix **X** with the coefficient vector **w** yields a **n x 1** prediction vector
- ![image-20200509160330024](Machine Learning.assets/image-20200509160330024.png)



- **loss function** capturing the sum of squared errors can be rewritten as 
- ![image-20200509160400355](Machine Learning.assets/image-20200509160400355.png)



#### Mean Squared Errors (MSE)

- Assesses the prediction quality of a regression model
- ![image-20200509160549363](Machine Learning.assets/image-20200509160549363.png)



### 2.4 Handling Non-Numerical Features

- Aims to make **non-numerical** features accessible to regression analysis



#### One-Hot Encoding

- Converts **nominal features** (e.g origin of a car) to numerical features
- For each value of the original feature, a binary feature is introduced, indicating whether a data point has the corresponding value for the feature

![image-20200509160819426](Machine Learning.assets/image-20200509160819426.png)



![image-20200509160924011](Machine Learning.assets/image-20200509160924011.png)





#### Handling Non-Numerical Features

- **Ordinal features** can be mapped to **integer values** preserving their ordern
- ![image-20200509161014624](Machine Learning.assets/image-20200509161014624.png)

- **Note: ** implicitly assumes that the **differences **between **adjacent values** are **uniform** (same magnitude)



### 2.5 Polynomial Regression

- Aims to solve the problem that occurs when **the relationship** between **target feature $y$** and the independent feature is **more complicated**
- Allows us to estimate the optimal coefficients of a **polynomial** having degree d
  - ![image-20200509161546527](Machine Learning.assets/image-20200509161546527.png)
- To estimate the coefficients $w_i$, we can **precompute the values** $x^{i}$ and treat them like other numerical features



### 2.6 Evaluation Fundamentals

- So far, we've **assessed** the **prediction quality** of our model based on the **same data** that we **used for training**
- **Bad Idea, because**
  - we can **not accurately measure** how well our model works for **previously unseen** data points
  - our model may **overfit** to the training data and lose its ability to make predictions



#### Overfitting

- Occurs when our model described the **training data** very **accurately**, but **fails to make predictions** for previously **unseen data points**
- **Likely, when Number of features** large in comparison to **number of data points** available
- -> Many features, more complex but the model fails to generalise



#### Avoiding Overfitting

- Assessing the quality of a model based on **test data** not in the **training data**
  - -> Split available data randomly into **training**, *validation*, and ***test data***



#### Training-Validation-Test-Splitting

- Common Approach to **reliably assess the quality** of a machine learning model and **avoiding overfitting** 
- Split into
  - **training data (~70% for optimal coefficients)**
  - *validation data* (typically about 20% of the data)
    - used for model selection and fixing
  - ***test data*** (typically 10%)
    - measure quality



#### k-Fold Cross Validation

- Another approach to quality test and avoid overfitting
- **Useful, when only limited data is available**
- **Data** is (randomly) **split** into **k folds** of (about) **equal size**
- k rounds of training and validation are performed, in which
  - **(k -1) folds** serve as **training data**
  - **one fold** serves as **validation / test data**
- In the end, the **MSE (mean of quality measure)** is reported to estimate overall quality



##### Example

![image-20200509162955411](Machine Learning.assets/image-20200509162955411.png)



#### Bias-Variance Tradeoff

- When performing **model selection** (choosing the degree for polynomial regression), we trade off between
  - **bias** as the error made due to simplifying assumptions (ex: linear relationship)
  - **variance** as the error made due to the model being sensitive to variations in the training data



![image-20200509163112110](Machine Learning.assets/image-20200509163112110.png)



### 2.7 Regularization

- Also: shrinkage
- Modifies **loss function** by adding a term reflecting the **complexity of the model**
  - Eg.: How many features have non-zero coefficients
- To trade off prediction quality and model complexity



#### Ridge Regression

- Modifies the loss function as follows
- ![image-20200509165124494](Machine Learning.assets/image-20200509165124494.png)
- With:
  - ![image-20200509165134181](Machine Learning.assets/image-20200509165134181.png)
- And $\lambda > 0$ as a tunable parameter



#### LASSO

- Modifies the loss function as follows
- ![image-20200509165215585](Machine Learning.assets/image-20200509165215585.png)







## 3. Classification

### Motivation

- Aims at predicting **nominal target features** based on **one or multiple** other **nominal** features



### Logistic Regression

- Simple-yet-popular classification method that builds on multiple linear regression
- **Data Matrix** X encodes **n data points** each of which has values for ***m* numerical features**
  - ![image-20200524140301087](Machine Learning.assets/image-20200524140301087.png)
- Initial focus on **Binary Classification** (target vector contains only {0,1})



#### How it works

- Logistic regression **predicts values** in [0,1], each of which can be interpreted as the **probability** that the corresponding **data point** belongs to class **1** 
- To determine the **predicted class** in {0,1} of a data point, we can **threshold** based on this probability as follows
  - ![image-20200524140539075](Machine Learning.assets/image-20200524140539075.png)



#### Assumptions

- LR assumes a **linear relationship** between the **log of odds** of the datapoint belonging to class **1** and the **numerical features**
  - ![image-20200524140629846](Machine Learning.assets/image-20200524140629846.png)
- as a **linear combination** of the features for data point **$x^{(i)}$** with **coefficients** $w_i$ that we need t odetermine



#### Determining the coefficient vector

- we search for 
  - ![image-20200524140812507](Machine Learning.assets/image-20200524140812507.png)
- **Different choices** of the **coefficients** correspond to **different** separating **hyperplanes** (straight lines), so that data points from the two classes lie on **different sides** of the hyperplane
- Example
  - ![image-20200524140928024](Machine Learning.assets/image-20200524140928024.png)



#### Loss Function

- We need it to measure how well a specific choice of the coefficients fits our data
- Recall that **predicted values** can be interpreted as **probabilities** that the DP belongs to **class 1**
- ![image-20200524141035349](Machine Learning.assets/image-20200524141035349.png)



##### Intuitive Loss Function

- Choosing coefficients in a way that DP from **class 1** have a **high probability** and DP from **class 0** do have low **probability**
- ![image-20200524141129087](Machine Learning.assets/image-20200524141129087.png)
- $G(w)$ is the **likelihood** that our regression model **predicts** the **classes** of our DPs correctly for a specific choice of coefficient w





### One-vs-Rest Classification

#### Motivation

- Using a **binary classification** for **more than two** classes
- Assumption: target feature assumes values in {0,...k-1} -> **k different classes**
- **One-vs-Rest** Classification trains a **classifier for each of the k classes**, distinguishing it for all the other classes
- A **DP** is put into the **class** whose classifier reports the highest score



#### Example

![image-20200524141503135](Machine Learning.assets/image-20200524141503135.png)



### Evaluating Classifiers

- How to assess the **prediction quality** of a classifier?



#### Confusion Matrix

- shows the performance of a classifier

  ![image-20200524141636525](Machine Learning.assets/image-20200524141636525.png)



#### Accuracy and Error Rate

##### Accuracy

- Measures the classifiers ability to put DP into the right class

![image-20200524141725875](Machine Learning.assets/image-20200524141725875.png)



##### Error Rate

- Counterpart to accuracy, reflects to what extent the classifier puts data point into the wrong class

  ![image-20200524141754629](Machine Learning.assets/image-20200524141754629.png)



##### Confusion Matrix

![image-20200524143349750](Machine Learning.assets/image-20200524143349750.png)







#### False Positive Rate and True Positive Rate

##### False positive Rate

- Fraction of the negative (0 / No) data points that is falsely classified as positive (1/Yes)

![image-20200524141838881](Machine Learning.assets/image-20200524141838881.png)



##### True Positive Rate

- Fraction of the positive (1 / Yes) points that is correctly classified as positive (1 / Yes)

  ![image-20200524141936262](Machine Learning.assets/image-20200524141936262.png)



##### Confusion Matrix

![image-20200524143412363](Machine Learning.assets/image-20200524143412363.png)







#### Precision and Recall

##### Precision

- Reflects the classifiers ability to **correctly detect positive** (1 / Yes) data points

![image-20200524142358918](Machine Learning.assets/image-20200524142358918.png)



##### Recall

- Reflects the classifiers ability to detect **all positive** (1 / Yes) data points

![image-20200524142428270](Machine Learning.assets/image-20200524142428270.png)





##### Confusion Matrix

![image-20200524143427572](Machine Learning.assets/image-20200524143427572.png)





#### F1-Measure

- Precision and Recall are widely used in IR
- **F1-Measure** as the **harmonic mean of precision and recall**

![image-20200524143232791](Machine Learning.assets/image-20200524143232791.png)



#### Micro and Macro Averages

- When **dealing** with **more than two classes**, the confusion matrix has **one column** and **one row per class**

![image-20200524143515716](Machine Learning.assets/image-20200524143515716.png)

- For each class **i** we can now determine the numbers $TN_i, FP_i, FN_i, TP_i$ assuming that it is the **positive class** and **all others are ** treaded as negative



##### Example

![image-20200524143738218](Machine Learning.assets/image-20200524143738218.png)



##### Micro Averages

- Plug per class numbers into the definitions

![image-20200524143811804](Machine Learning.assets/image-20200524143811804.png)



##### Macro Averages

- Average per class quality assessments

![image-20200524143832369](Machine Learning.assets/image-20200524143832369.png)





### 3.3 k-Nearest-Neighbours

- **kNN** is another simple-yet-popular classification method that often serves as a **baseline**
- Is a **lazy learning method** -> does not actually learn parameters of a model, but always looks at the **training data**
  - -> **no cost** for training a model
  - -> **cost at runtime** (e.g when classifying a data point)
    - depends on **amount of training data**

#### How it works

- To classify a previously unseen data point **kNN**
  - identifies the $k$ **closes data points** in the training data according to a **suitable distance measure**
  - predicts the **nominal target feature** (class) as the **value** that is **most frequent** among the $k$ closes dp



#### Distance Methods

##### Minkowski Distance

![image-20200607131526061](Machine Learning.assets/image-20200607131526061.png)



![image-20200607131539650](Machine Learning.assets/image-20200607131539650.png)



##### Manhattan Distance

![image-20200607131605564](Machine Learning.assets/image-20200607131605564.png)



##### Euclidian Distance

![image-20200607131623975](Machine Learning.assets/image-20200607131623975.png)



##### Norminalization and Standardization

- Magnitudes of features matter
- ![image-20200607131747079](Machine Learning.assets/image-20200607131747079.png)
- To avoid features with **larger magnitude** dominating the **distances**, it makes sense to **normalize and standardize**



#### Normalization: Min-Max

![image-20200607131856508](Machine Learning.assets/image-20200607131856508.png)



#### Standardization

![image-20200607131914399](Machine Learning.assets/image-20200607131914399.png)





### Naive Bayes

- Often used for **text classification**
- supports an **arbitrary numbers of classes**
- Name: **Bayes theorem** is used with **naive independence assumption** about the data



#### Events and Probabilities

![image-20200607132105413](Machine Learning.assets/image-20200607132105413.png)



#### Conditional Probabilities

![image-20200607132122142](Machine Learning.assets/image-20200607132122142.png)



#### Independence

![image-20200607132158423](Machine Learning.assets/image-20200607132158423.png)



#### Bayes theorem

![image-20200607132214817](Machine Learning.assets/image-20200607132214817.png)



![image-20200607132226846](Machine Learning.assets/image-20200607132226846.png)



#### Bags of Words Model

![image-20200607132310939](Machine Learning.assets/image-20200607132310939.png)



#### Data Matrix

![image-20200607132322107](Machine Learning.assets/image-20200607132322107.png)



#### Naive Bayes for Text Classification

![image-20200607132347677](Machine Learning.assets/image-20200607132347677.png)



![image-20200607132358739](Machine Learning.assets/image-20200607132358739.png)



#### Class Priors in Naive Bayes

![image-20200607132417157](Machine Learning.assets/image-20200607132417157.png)



#### Conditional Probabilities in Naive Bayes

![image-20200607132550524](Machine Learning.assets/image-20200607132550524.png)

![image-20200607132604187](Machine Learning.assets/image-20200607132604187.png)

![image-20200607132610621](Machine Learning.assets/image-20200607132610621.png)



#### Usage

![image-20200607132633289](Machine Learning.assets/image-20200607132633289.png)

![image-20200607132642687](Machine Learning.assets/image-20200607132642687.png)

