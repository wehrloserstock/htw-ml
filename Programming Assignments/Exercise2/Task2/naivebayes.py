import math
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
import os
import io
# load data

def extract_class_priors(nb: MultinomialNB, vectorizer):
    # print class priors
    print("--- CLASSES ---\n")
    for c in range(0, len(nb.classes_)):
        print('Class: ' + str(c) + " -> " + nb.classes_[c])
        print('Value:' + str(math.exp(nb.class_log_prior_[c])))
    print()
    print("--- PROPABILITIES --- ")
    feature_names = vectorizer.get_feature_names()
    print("Feature Name length: " + str(len(feature_names)))

    #Get feature propabilities
    feature_probs = pd.DataFrame(nb.feature_log_prob_)

    #calculate log(P[v | spam] / P[v | ham])
    spam_probs = (feature_probs.iloc[1] / feature_probs.iloc[0]).apply(np.log)
    #calculate log(P[v | ham] / P[v | spam])
    ham_probs = (feature_probs.iloc[0] / feature_probs.iloc[1]).apply(np.log)

    print("Class: spam")
    print("Index   Word   Propability")
    for idx, (sidx, prop) in enumerate(ham_probs.nlargest(10).iteritems()):
        w = feature_names[sidx]
        print(f"{idx}.: {w} -> {prop}")


    print("Class: ham")
    print("Index   Word   Propability")
    for idx, (sidx, prop) in enumerate(spam_probs.nlargest(10).iteritems()):
        w = feature_names[sidx]
        print(f"{idx}.: {w} -> {prop}")

    print()



def print_quality_measures(validation_data, predictions, pos_label):
    # calculate measurements
    ap = precision_score(y_true=validation_data, y_pred=predictions, pos_label=pos_label)

    ar = recall_score(y_true=validation_data, y_pred=predictions, pos_label=pos_label)

    f1 = f1_score(y_true=validation_data, y_pred=predictions, pos_label=pos_label)

    # print title
    print(f'---PREDICTION MEASURES for \"{pos_label}\"---\n')
    # print these measures
    print(f'Precision: {ap}\nRecall: {ar}')
    print(f'f1: {f1}\n')

def do_ml(fp):

    df = pd.read_csv(fp, header=0)

    X = df.iloc[:,1].values
    X_clean = X[pd.notnull(X)]
    y = df.iloc[:,0].values
    y_clean = y[pd.notnull(X)]
    # convert documents into bags-of-words
    vectorizer = CountVectorizer()
    X_cnt = vectorizer.fit_transform(X_clean)

    # split into training data (80%) and test data (20%)
    X_train, X_test, y_train, y_test = train_test_split(X_cnt, y_clean,
                                                        test_size=0.2, random_state=0)
    # train naive bases classifier
    nb = MultinomialNB(alpha=0.0001)
    nb.fit(X_train, y_train)

    # predict labels
    y_predicted = nb.predict(X_test)

    # compute confusion matrix
    print("---Confusion Matrix ---")
    print(confusion_matrix(y_true=y_test, y_pred=y_predicted))

    print_quality_measures(validation_data=y_test, predictions=y_predicted, pos_label="spam")
    print_quality_measures(validation_data=y_test, predictions=y_predicted, pos_label="ham")


    return nb, vectorizer
def clean_file(filename):

    data = ""

    with open(filename, "r", encoding="ASCII", errors="ignore") as fp:
        for idx, line in enumerate(fp):
            line = line.strip()
            data += line + "\n"

        return data


if __name__ == '__main__':
    data = clean_file("spam.csv")
    #tell python to treat data string as a file
    data_as_string_io = io.StringIO(data)
    model, vectorizer = do_ml(data_as_string_io)
    extract_class_priors(model, vectorizer)
