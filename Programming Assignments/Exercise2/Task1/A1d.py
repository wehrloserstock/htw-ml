# -*- coding: utf-8 -*-
"""
Created on Sun Jun  7 09:21:07 2020

@author: RP, JK
"""


import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler


data = pd.read_csv("./letter-recognition.data",
                 sep = ",",
                 index_col = 0,
                 decimal = ".")

Letter = data.iloc[:,0].values     # returns the letter
Rest = data.iloc[:,1:].values      # returns the rest

Rest_training, Rest_test, Letter_training, Letter_test = train_test_split(Rest, Letter, test_size = 0.2, random_state = 0)

#d)
#normalized training data
mms = MinMaxScaler()
mms.fit(Rest_training)
Rest_training_normalized = mms.transform(Rest_training)
Rest_test_normalized = mms.transform(Rest_test)

#kNN with k = 7
knc = KNeighborsClassifier(n_neighbors = 7)
knc.fit(Rest_training_normalized, Letter_training)
prediction = knc.predict(Rest_test_normalized)

#error rate
accuracy = accuracy_score(y_true = Letter_test, y_pred = prediction)
error_Rate = 1.0 - accuracy
print("d)")
print("Error-Rate      = " + str(error_Rate))

# micro precision and recall
micro_precision = precision_score(y_true = Letter_test, y_pred = prediction, average = 'micro')
micro_recall = recall_score(y_true = Letter_test, y_pred = prediction, average = 'micro')
print("Micro-Precision = " + str(micro_precision))
print("Micro-Recall    = " + str(micro_recall))

# macro precision and recall
macro_precision = precision_score(y_true = Letter_test, y_pred = prediction, average = 'macro')
macro_recall = recall_score(y_true = Letter_test, y_pred = prediction, average = 'macro')
print("Macro-Precision = " + str(macro_precision))
print("Macro-Recall    = " + str(macro_recall))
print("")


