# -*- coding: utf-8 -*-
"""
Created on Sun Jun  7 09:21:07 2020

@author: RP
"""


import pandas as pd
from sklearn.model_selection import train_test_split

data = pd.read_csv("./letter-recognition.data",
                 sep = ",",
                 index_col = 0,
                 decimal = ".")

Letter = data.iloc[:,0].values     # returns the letter
Rest = data.iloc[:,1:].values      # returns the rest

Rest_training, Rest_test, Letter_training, Letter_test = train_test_split(Rest, Letter, test_size = 0.2, random_state = 0)


