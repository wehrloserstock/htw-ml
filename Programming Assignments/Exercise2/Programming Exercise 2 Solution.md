 

Julian Krieger, 3761258

Roman Pinkel, 3761932



# Programming Exercise 2

## Task 2

### a)  Preprocessing

For pre-processing our data, we look into the encoding of our file.

![image-20200607140529030](Programming Exercise 2.assets/image-20200607140529030.png)

Checking with `file -I` further proves that there are some lines with incorrectly encoded characters

<img src="Programming Exercise 2.assets/image-20200607140737337.png" alt="image-20200607140737337" style="zoom:50%;" />



Python3 and above opens file with an encoding supplied by the operating system by default (utf-8 in my case). Trying to read `spam.csv` results in an error. To fix this, we can supply a preferred encoding (ASCII) and an error handling method to python's `open`. 



### b)

```
---PREDICTION MEASURES for "spam"---

Precision: 0.975
Recall: 0.9397590361445783
f1: 0.9570552147239264

---PREDICTION MEASURES for "ham"---

Precision: 0.9895287958115183
Recall: 0.9957850368809273
f1: 0.9926470588235293

```



### c)

In scikit, we can access the probability P(v|w) for a word v in the class w with the `feature_log_probs_` attribute. To better use this, we put the resulting `ndarray` into a dataframe. This allows us to not only use handy `pandas` features (such as a reimplementation of the division operator on series), but also to use `Series.nglargest` to find the `n` largest numbers in a series.



#### Output

```
--- CLASSES ---

Class: 0 -> ham
Value:0.869643257796725
Class: 1 -> spam
Value:0.13035674220327576

--- PROPABILITIES --- 
Feature Name length: 8637
Class: spam
Index   Word   Propability
0.: claim -> 1.3823290541102595
1.: prize -> 1.3335485235608164
2.: 150p -> 1.3016750955406289
3.: tone -> 1.2607960345239724
4.: 18 -> 1.2284574564636026
5.: 500 -> 1.2195194040939887
6.: guaranteed -> 1.2195194040939887
7.: cs -> 1.205328076963947
8.: awarded -> 1.1791659109345
9.: 1000 -> 1.173485278485109
Class: ham
Index   Word   Propability
0.: gt -> 1.262473656951832
1.: lt -> 1.2609537211392612
2.: he -> 1.2000862033732866
3.: she -> 1.1556915724734838
4.: lor -> 1.1519454183975322
5.: da -> 1.142925465486931
6.: sorry -> 1.1389325854777472
7.: later -> 1.125013710690099
8.: wat -> 1.0868897198982628
9.: happy -> 1.0815394402759082
```



#### Evaluating Results

When looking at the results for **spam**, we can see that the naive Bayes model found a list of words one could intuitively assign to common spam mails. Words like **claim, price, guaranteed** and **awarded** are often found in spam mails containing misleading advertisements. The numbers **150, 500** and **1000** fit this model if we think about them in a monetary way. 



When looking at the results for **ham**, however, we find common **conjunctions** used in text messages as well as gender pronouns. If we wanted a more precise presentation of non stop-words, we could filter them from the bag-of-words.



### d)

#### Encoding

Instead of stripping stop-words or words we don't think are relevant, we can apply **tf-idf** term weighting, to give more weight to words appearing less frequently in our document collection. To do this, we can either apply a `TfidfVectorizer` to our initial dataset instead of the `CountVectorizer`, or alternatively apply a `TfidfTransformer` to our count matrix.



#### Distance Measure

According to [MIT](https://www.youtube.com/watch?v=h0e2HAPTGF4) and [Charu C. Aggarwal, Alexander Hinneburg, and Daniel A. Keim](https://bib.dbvis.de/uploadedFiles/155.pdf), the Manhattan Distance may be preferable to the Euclidian Distance if the dataset features a high number of dimensions. Because our dataset is 2 dimensional only, we use the **Euclidian Distance** measure and thus set $p=2$



#### Result

```
---PREDICTION MEASURES for "spam"---
Precision: 1.0
Recall: 0.23493975903614459
f1: 0.3804878048780488

---PREDICTION MEASURES for "ham"---
Precision: 0.8819702602230484
Recall: 1.0
f1: 0.937283950617284

```



#### Referencing 1.1.3.2

Removing the stopwords (according to the english language) yields somewhat better results

```
---PREDICTION MEASURES for "spam"---
Precision: 1.0
Recall: 0.1927710843373494
f1: 0.32323232323232326

---PREDICTION MEASURES for "ham"---
Precision: 0.876269621421976
Recall: 1.0
f1: 0.9340551181102362

```





