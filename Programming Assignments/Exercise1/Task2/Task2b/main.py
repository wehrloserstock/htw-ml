import os
from typing import Tuple

import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import KFold, cross_validate
from sklearn.metrics import mean_squared_error
import numpy as np
import math
import datetime


def split_df(df: pd.DataFrame) -> Tuple[pd.DataFrame, pd.Series]:
    target = df.loc[:, "Value"]
    base = df.loc[:, df.columns != "Value"]

    return base, target

def training_round(train: pd.DataFrame, test: pd.DataFrame, model) -> float:

    train_base, train_target = split_df(train)
    test_base, test_target = split_df(test)

    model.fit(X=train_base.values, y=train_target.values)

    predictions = model.predict(test_base.values)

    coreff = np.corrcoef(predictions,  test_target.values)
    rmse = math.sqrt(mean_squared_error(test_target.values, predictions))

    return rmse

def number_format(number, decimal_places=2) -> str:
    return '{0:,}'.format(round(number, decimal_places)).replace('.', '~').replace(',', '.').replace('~', ',')
    pass

def get_train_test(df: pd.DataFrame, splits=5):
    kfolds = KFold(n_splits=splits)
    for train, test in kfolds.split(df):
        print("train: \n%s, \ntest: \n%s" % (df.iloc[train], df.iloc[test]))
        yield df.iloc[train], df.iloc[test]

def convert_date(date: str):
    if(type(date) != str):
        return 1 #values explode if I dont
    start_date = datetime.datetime.strptime(date, "%b %d, %Y")
    end_date = datetime.datetime.now()

    num_months = (end_date.year - start_date.year) * 12 + (end_date.month - start_date.month)
    return num_months

def number_format_euro(num: str) -> float:

    if not num:
        print(num)
        return 0

    num = num.replace("€", "")
    replacer = 0
    if "M" in num:
        replacer = 10000000
        num = num.replace("M", "")

    if "K" in num:
        replacer = 1000
        num = num.replace("K", "")
    rounded = math.floor(float(num))

    finalnum = rounded * replacer
    return finalnum

if __name__ == "__main__":

    fifa = pd.read_csv("./data.csv", header=0, sep=',', index_col=0)

    reg = linear_model.LinearRegression()

    Age = fifa.Age
    Overall = fifa.Overall
    Potential = fifa.Potential
    InternatioalRep = fifa["International Reputation"].rename("InternationalReputation").fillna(0)

    Weight = fifa.Weight.replace(['[^.0-9]'], '', regex=True).fillna(0)
    Height = fifa.Height.replace(['[^.0-9]'], '', regex=True).fillna(0)

    #This is new
    Club = pd.get_dummies(fifa.Club)
    monthsActive = fifa.Joined.apply(convert_date)


    Value = fifa.Value.fillna(0).apply(number_format_euro).astype(int)

    x = filter(lambda x: type(x) != int, Value)

    dataframe = pd.concat([Age, Overall, Potential, InternatioalRep, Weight, Height, Club, monthsActive, Value], axis=1)

    mses = []

    for train, test in get_train_test(dataframe):
        rmse_x = training_round(train, test, reg)
        mses.append(rmse_x)

    mean = np.mean(mses)

    print(f'RMSE Values: {[number_format(i) + "€" for i in mses]}\n')
    print(f'Mean: {number_format(mean) + "€"}')
