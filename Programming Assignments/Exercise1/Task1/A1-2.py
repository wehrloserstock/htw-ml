# -*- coding: utf-8 -*-
"""
Spyder Editor

Dies ist eine temporäre Skriptdatei.
"""


import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import pearsonr


total = pd.read_csv('D:/HTW/Semester 6/Machine Learning/Übungen/programming/happiness-report-2016.csv',
                 sep=",",
                 index_col=0,
                 decimal=".")


data1 = total.iloc[:,3].values    # returns the happyness
data2 = total.iloc[:,8].values     # returns the other data


corr, _ = pearsonr(data1, data2)
print('Pearsons correlation: %.3f' % corr)

plt.scatter(data2, data1, color='blue', marker='x')
plt.xlabel('[Family]')
plt.ylabel('[Happiness]')


plt.text(0.13, 7.3, 'Pearsons correlation: %.3f' % corr, ha="center", va="center",  bbox=dict(boxstyle="round",
                   ec=(1., 0.8, 0.5),
                   fc=(0.5, 0.8, 0.5),
                   ))

plt.show()

