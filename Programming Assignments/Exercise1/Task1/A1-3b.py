# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
from sklearn.linear_model import LinearRegression


total = pd.read_csv('D:/HTW/Semester 6/Machine Learning/Übungen/programming/happiness-report-2016.csv',
                 sep=",",
                 index_col=0,
                 decimal=".")


data1 = total.iloc[:,3].values    # returns the happyness
data2 = total.iloc[:,9].values     # returns the other data


corr, _ = pearsonr(data1, data2)
print('Pearsons correlation: %.3f' % corr)

plt.scatter(data2, data1, color='blue', marker='x')
plt.xlabel('[Health]')
plt.ylabel('[Happiness]')


plt.text(0.4, 2.9, 'Pearsons correlation: %.3f' % corr, ha="center", va="center",  bbox=dict(boxstyle="round",
                   ec=(1., 0.8, 0.5),
                   fc=(0.5, 0.8, 0.5),
                   ))

x = np.array(data1)
y = np.array(data2)

x = [] # we need an list of lists here
for i in data2:
    x.append([i])

y = data1

reg = LinearRegression()
reg.fit(x,y)

#reg.intercept_         # returns the optimal w0
#print(reg.intercept_)
reg.coef_[0]            # returns the optimal w1
print('W1  = ' + str(reg.coef_[0]))

plt.plot(x, reg.predict(x), color='red', linewidth=2)

plt.show()


# Mean Squared Error 
MSE = np.square(np.subtract(x,reg.predict(x))).mean() 
print ('MSE = ' + str(MSE))
