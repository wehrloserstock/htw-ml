import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.model_selection import KFold, cross_validate
from sklearn.metrics import mean_squared_error
import numpy as np
import math
import datetime
from sklearn.model_selection import train_test_split


total = pd.read_csv("2016.csv",
    sep=",",
    decimal=".",
    header=0
)


happiness = total.iloc[:,3].values    # returns the happyness

Region = pd.get_dummies(total.iloc[:,1])
gdp = total.iloc[:,7]    # returns the gdp
family = total.iloc[:,8]     # returns the family
health = total.iloc[:,9]     # returns the health
freedom = total.iloc[:,10]     # returns the freedom
trust = total.iloc[:,11]    # returns the trust

dataframe = pd.concat([gdp, family, health, freedom, trust, Region], axis=1)

X_Train, X_test, y_train, y_test = train_test_split(dataframe, happiness, test_size=0.2, shuffle=True)

model = linear_model.LinearRegression()
model.fit(X=X_Train, y=y_train)
predictions = model.predict(X_test)
optimal_coefficients = model.coef_
mse = mean_squared_error(
    y_pred=y_test, y_true=predictions
)

print(optimal_coefficients)
print(mse)